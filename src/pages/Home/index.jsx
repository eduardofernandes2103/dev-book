import Button from '../../components/Button'
import Container from './styles'
import { useHistory } from 'react-router'

const Home = () => {
    const history = useHistory();
    
    const handleRedirection = (path) => {
        return history.push(path)
    }
    
    return (
    <Container>
        <div className="title">
            <h1>Dev<b>Book</b></h1>
        </div>
        <h3>A verdadeira rede social Dev</h3>
        <div className="buttons">
            <Button 
                setColor="white"
                setFontColor="black"
                setSize="large"
                click={() => handleRedirection('/signup')}
            >SignUp</Button>
            <Button 
                setColor="black"
                setFontColor="white"
                setSize="large"
                click={() => handleRedirection('/login')}
            >Login</Button>
        </div>
    </Container>
    )
}

export default Home;
