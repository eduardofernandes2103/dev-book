import styled from 'styled-components';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    justify-content: center
    align-items: center;
    text-align:center

    .title{
        margin-top: 10rem;
        text-align: center;
    }

    h1{
        margin-top: 10rem;
        text-align: center;
    }

    h3{
        margin-top: 1rem;
        margin-bottom: 4rem;
        text-align: center
    }

    .buttons{
        text-align: center;
    }

    b{
        color: #cc6600;
    }
`
export default Container