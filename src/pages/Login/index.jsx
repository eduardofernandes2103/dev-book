import Container from './styles'
import Input from '../../components/Input'
import Button from '../../components/Button'
import { useForm } from 'react-hook-form'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import api from '../../services/api'
import { Link, useHistory } from 'react-router-dom'
import {toast, ToastContainer}  from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';

const Login = ({ authenticated, setAuthenticated }) => {
    const formSchema = yup.object().shape({
        email: yup
                    .string()
                    .email("Email Inválido")
                    .required("Email obrigatório"),
        password: yup
                            .string()
                            .min(6)
                            .required("Senha obrigatória"),
    });

    const {
            register,
            handleSubmit,
            formState : { errors }
            } = useForm({
                resolver: yupResolver(formSchema)
            })

            const history = useHistory()

            const onSubmitFunction = (data) => {
                api
                .post('/sessions', data)
                .then(response => {
                    const { token, user } = response.data;

                    localStorage.setItem('@devBook:token', JSON.stringify(token));

                    localStorage.setItem('@devBook:user', JSON.stringify(user));

                    setAuthenticated(true);

                    return history.push('/dash');
                })
                .catch((err) => toast.error('Tente novamente'));
            }
    
    return (
        <Container>
            <ToastContainer />
            <div className="loginTitle">
                <h2>Login</h2>
            </div>
            <form onSubmit={ handleSubmit(onSubmitFunction) }>
                <Input register={register} name="email" placeholder="E-mail" />
                    <span>{errors.email?.message}</span>
                <Input register={register} name="password" placeholder="Inserir Senha" type="password" />
                    <span>{errors.password?.message}</span>
                
                <Button setColor="black" setFontColor="white" setSize="medium" type="submit"> Enviar</Button>
            </form>
                <p>Ainda não possuir uma conta? <Link to="/signup"><b>Cadastre-se</b></Link></p>
        </Container>
    )
}

export default Login