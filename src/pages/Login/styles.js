import styled from 'styled-components';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    justify-content: center
    align-items: center;
    text-align:center

    .loginTitle{
        margin-top: 2rem;
        text-align: center;
    }

    h2{
        margin-top: 2rem;
        text-align: center;
    }
    form{
        display: flex;
        flex-direction: column;
        flex-wrap: wrap;
        justify-content: center;
        align-items: center;
        text-align: center;
        margin-bottom: 2rem;
    }

    .loginText{
        display: flex;
        flex-direction: row;
        justfy-content: center;
        align-items: center;
        text-align: center;
        flex-wrap: nowrap;
        margin-top: 2rem;
    }
    p{
        text-align: center;
        margin-top: -1rem;
    } 
    span{
        color: red;
    }
    a{
        color: #cc6600;
        text-decoration: none;
    }
`
export default Container