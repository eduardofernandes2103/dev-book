import styled from 'styled-components';

const Container = styled.div`
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    justify-content: center
    align-items: center;
    text-align:center

    .dashTitle{
        display: flex;
        flex-direction: row;
        margin-top: 2rem;
        text-align: center;
    }

    h2{
        margin-top: 2rem;
        text-align: center;
    }
    form{
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
        align-items: center;
        text-align: center;
        margin-bottom: 2rem;
    }

    .cards{
        display: flex;
        flex-direction: row;
        justfy-content: center;
        align-items: center;
        text-align: center;
        flex-wrap: nowrap;
        margin-top: 2rem;
    }

    .buttonArea{
        margin-top: 3rem;
        text-align: center;
    }
`
export default Container