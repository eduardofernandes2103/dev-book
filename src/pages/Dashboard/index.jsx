import { useHistory } from 'react-router'
import { useForm } from 'react-hook-form'
import { useState, useEffect } from 'react'
import api from '../../services/api'
import Container from './styles'
import SkillCard from '../../components/SkillCard'
import Input from '../../components/Input'
import Button from '../../components/Button'
import Select from '../../components/Select'
import {toast, ToastContainer}  from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';

const Dashboard = ({ authenticated }) => {

    const [hardSkills, setHardSkills] = useState([]);

    const [token] = useState(JSON.parse(localStorage.getItem("@devBook:token")) || "")
    
    const [user] = useState(JSON.parse(localStorage.getItem("@devBook:user")))

    const { register, handleSubmit } = useForm();

    const loadHardSkills = (data) => {
        api
            .get(`users/${user.id}`)
            .then(response => setHardSkills(user.techs))
    }
    
    const history = useHistory();
    
    const handleRedirection = (path) => {
        
        localStorage.clear();

        return history.push(path)
    }
    
    useEffect (() => {
        loadHardSkills()
    }, []);

    const onSubmitFunction = (data) => {
        api
        .post("/users/techs", data, {
            headers: { 
                Authorization: `Bearer ${token}`
            }
        }).then(response => setHardSkills([...hardSkills, data]) 
        ).then(toast.success('Skill Adicionada'))
    };

    const deleteSkill = (id) => {
        const newHardSkills = hardSkills.filter((skill) => skill.id !== id);

        api.delete(`/users/techs/${id}`, {
            headers: { 
                Authorization: `Bearer ${token}`
            } 
        }).then(response => setHardSkills(newHardSkills)
        ).then(toast.success("Skill removida com sucesso")
        ).then(loadHardSkills())
    }

    return (
        <Container>
            <ToastContainer />
            <div className="dashTitle">
                <h2>Cadastre até seis Hard-Skill's</h2>
            </div>
            <form onSubmit={ handleSubmit(onSubmitFunction) }>
                <Input register={register} name="title" placeholder="Nome da Skill que será adicionada" />
                <Select 
                register={register}
                name="status" 
                value1="Iniciante"
                value2="Intermediário"
                value3="Avançado"
                children1="Iniciante"
                children2="Intermediário"
                children3="Avançado"
                />
                <Button 
                        setColor="white" 
                        setFontColor="black" 
                        setSize="medium"
                        type="submit" 
                > Adicionar</Button>
            </form>
            <div className="cards">
                {
                    hardSkills.map((skill, index) => (
                        
                            <SkillCard 
                                        key={index}
                                        id={skill.id} 
                                        technology={skill.title} 
                                        level={skill.status} 
                                        click={() => deleteSkill(skill.id)}
                            />
                        
                    ))
                }
            </div>
            <div className="buttonArea">
                <Button 
                    setColor="black" 
                    setFontColor="white" 
                    setSize="large"
                    click={() => handleRedirection('/')}
                >Logout</Button>
            </div>
            
        </Container>
    )
}

export default Dashboard
