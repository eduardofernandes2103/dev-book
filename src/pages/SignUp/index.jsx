import Container from './styles'
import { useForm } from 'react-hook-form'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import api from '../../services/api'
import Input from '../../components/Input'
import Select from '../../components/Select'
import Button from '../../components/Button'
import { Link, useHistory } from 'react-router-dom'

const SignUp = ({ authenticated }) => {
    const formSchema = yup.object().shape({
        
        email: yup
                    .string()
                    .email("Email Inválido")
                    .required("Email obrigatório"),
        password: yup
                            .string()
                            .min(6)
                            .required("Senha obrigatória"),
        name: yup.string().required("Nome obrigatório"),
        bio: yup.string().required("A autodescrição é obrigatório"),
        contact: yup.string().required("Linkedin obrigatório"),
    });

    const {
                register,
                handleSubmit,
                formState: { errors }
            } = useForm({
                resolver: yupResolver(formSchema)
            });

    const history = useHistory()

    const onSubmitFunction = ({name, email, password, bio, contact, course_module}) => {
        const user = {name, email, password, bio, contact, course_module};
        api
        .post("/users", user)
        .then((_) => {
            return history.push('/login')
        })
    };


    return (
        <Container>
            <div className="pageTitle">
                <h2>Cadastro</h2>
            </div>
            <form onSubmit={ handleSubmit(onSubmitFunction) }>
                <Input 
                        register={register}
                        placeholder="E-mail"  
                        name="email"
                />
                <span>{errors.email?.message}</span>
                <Input 
                        register={register} 
                        placeholder="Nome Completo" 
                        name="name" 
                />
                <span>{errors.name?.message}</span>
                <Input 
                        register={register} 
                        placeholder="Defina uma Senha" 
                        type="password" 
                        name="password" 
                />
                <span>{errors.password?.message}</span>
                <Input 
                        register={register} 
                        placeholder="Autodescrição" 
                        name="bio" 
                />
                <span>{errors.bio?.message}</span>
                <Input 
                        register={register} 
                        placeholder="Perfil no Linkedin" 
                        name="contact" 
                />
                <span>{errors.contact?.message}</span>
                <Select
                    register={register} 
                    name="course_module"
                    value1="Primeiro módulo (Introdução ao Frontend)"
                    value2="Segundo módulo (Frontend avançado)"
                    value3="Terceiro módulo (Introdução ao Backend)"
                    value4="Quarto módulo (Introdução ao Backend)"
                    children1="Primeiro módulo (Introdução ao Frontend)"
                    children2="Segundo módulo (Frontend Avançado)"
                    children3="Terceiro módulo (Introdução ao Backend)"
                    children4="Quarto módulo (Introdução ao Backend)"
                />
                <Button 
                    setColor="black" 
                    setFontColor="white" 
                    setSize="large"
                    type="submit"
                >Cadastrar</Button>
            </form>
                <p>Já possui cadastro? <Link to="/login"><b>Login</b></Link></p>
        </Container>
    )
}

export default SignUp