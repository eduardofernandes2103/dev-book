import {Card} from './styles';
import Button from '../Button';

const SkillCard = ({ technology, level, click }) => {
    return (
        <Card>
            <div>
                <p>{technology}</p>
                <hr />
                <p>{level}</p>
                <Button setColor={'red'}
                        setSize={'medium'}
                        setFontColor={"white"}
                        click={click}
                        >Apagar</Button>
            </div>
        </Card>
    )
}

export default SkillCard;
