import {Container} from './styles'

const Input = ({error, register, name, ...rest }) => {
    return (
        
        <Container> 
                {/* {!!error && <span>{error}</span>} */}
                <input {...register(name)} { ...rest }  />
        </Container>

    )
}

export default Input;
