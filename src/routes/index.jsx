import {Route, Switch} from 'react-router-dom';
import Home from '../pages/Home'
import SignUp from '../pages/SignUp'
import Login from '../pages/Login'
import Dashboard from '../pages/Dashboard'
import { useEffect, useState } from 'react'

const Routes = () => {
    const [authenticated, setAuthenticated] = useState(false);

    useEffect(() => {
        const token = JSON.parse(localStorage.getItem('@devBook:token'));

        if (token) {
            return setAuthenticated(true);
        }
    }, [authenticated])

    return (
        <Switch>
            <Route exact path='/'>
                <Home authenticated={authenticated} />
            </Route>
            <Route exact path='/signup'>
                <SignUp authenticated={authenticated} />
            </Route>
            <Route exact path='/login'>
                <Login
                    authenticated={authenticated}
                    setAuthenticated={setAuthenticated}
                />
            </Route>
            <Route exact path='/dash'>
                <Dashboard authenticated={authenticated} />
            </Route>
        </Switch>
    )
}

export default Routes;
